ROOT=${1}" at `date`"
mkdir "$ROOT"

cd "$ROOT"
mkdir about_me about_me/personal about_me/professional my_friends my_system_info

echo "https://twitter.com/${2}" > about_me/personal/twitter.txt
echo "https://linkedin.com/${3}" > about_me/professional/linkedin.txt

curl -o my_friends/list_of_my_friends.txt https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt

echo "My username: `id -u -n`" > my_system_info/about_this_laptop.txt
echo "With host: `uname -a`" >> my_system_info/about_this_laptop.txt
